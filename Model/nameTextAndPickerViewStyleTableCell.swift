//
//  CleaningBokingStyleTableViewCell.swift
//  thriptop-ios
//
//  Created by WEIQIANG LIANG on 5/12/2015.
//  Copyright © 2015 RMIT. All rights reserved.
//

import Foundation
import UIKit

class nameTextAndPickerViewStyleTableCell:UITableViewCell,UIPickerViewDataSource, UIPickerViewDelegate{

    @IBOutlet weak var nameTextOfUITableCell: UILabel!

    @IBOutlet weak var editableTextFieldOfUITableCell: UITextField!
    var namedTypeOfHousePickerView = UIPickerView();

    // define data source for the picker view
    let houseTypeData = ["Any","houseType1","houseType2","houseType3","houseType4",];

    //data structure of the pickerView
    // should be remove, not in the view!!!
    let numberOfBedRoomData = ["Any","1","2","3","4",];
    let numberOfToiletData = ["Any","1","2","3","4",];
    let numberOfLivingRoomData = ["Any","1","2","3","4",];


    @IBAction func startInputText(sender: UITextField) {
        //setting datasource and delegation when user is start to edit text.
        namedTypeOfHousePickerView.dataSource = self;
        namedTypeOfHousePickerView.delegate = self;

        //define a toolbar at the top of the edit view
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.Default
        toolBar.translucent = true
        toolBar.sizeToFit()
        //define a button to the toolbar, calling donePicker when button is clicked.
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.Done, target: self, action: "donePicker:")
        //add button to tool bar.
        toolBar.items = [doneButton];
        //setting the inputview of editing to use pickerView, and add the toolbar to the top of the edit view.
        editableTextFieldOfUITableCell.inputAccessoryView = toolBar;
        editableTextFieldOfUITableCell.inputView = namedTypeOfHousePickerView;

    }

    // MARK - pickerView Setting
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        //use switch to determine which datasource is required.
        switch (self.nameTextOfUITableCell.text!){
            case "房屋类型": return houseTypeData[row];
        default:
            return numberOfBedRoomData[row];
        }
    }

    // returns the number of 'columns' to display.
    @available(iOS 2.0, *)
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1;
    }

    // returns the # of rows in each component..
    @available(iOS 2.0, *)
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return houseTypeData.count;
    }

    // do something when user select a value in the pickerview
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        //use switch to determine which datasource is required.
        switch (self.nameTextOfUITableCell.text!){
        case "房屋类型": self.editableTextFieldOfUITableCell.text = houseTypeData[row];
        default:
            self.editableTextFieldOfUITableCell.text = numberOfBedRoomData[row];
        }
    }

    // called when user click on done in the pickerview.
    func donePicker(sender:UIBarButtonItem){
        self.namedTypeOfHousePickerView.removeFromSuperview();
        self.editableTextFieldOfUITableCell.endEditing(true);
    }

}
