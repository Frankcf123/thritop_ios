//
//  Utility.swift
//  thriptop-ios
//
//  Created by Chen Fang on 4/12/2015.
//  Copyright © 2015 RMIT. All rights reserved.
//

import Foundation
import UIKit
import Alamofire


class Utility{
    func showAlertBox(alertText:String){
        let alert=UIAlertView()
        alert.title=alertText
        alert.message=alertText
        alert.addButtonWithTitle("OK")
        alert.show();
    }
    
    func recordDefaultValue(key:String,value:String){
        NSUserDefaults.standardUserDefaults().setObject(value, forKey: key)
    }
    
    func getDefaultValue(key:String) -> String{
        let returnvalue = NSUserDefaults.standardUserDefaults().stringForKey(key)
        if(returnvalue == nil){
            return ""
        }else{
            return returnvalue!
        }
    }
    
    
  
}