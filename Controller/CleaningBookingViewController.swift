//
//  CleaningBookingViewController.swift
//  thriptop-ios
//
//  Created by WEIQIANG LIANG on 5/12/2015.
//  Copyright © 2015 RMIT. All rights reserved.
//

import Foundation
import UIKit

class CleaningBookingViewController:UIViewController,UITableViewDataSource,UITableViewDelegate{

    let tableViewDataSource = ["房屋类型","睡房数量","客厅/饭厅 数量","浴室数量","洗手间/厕所 数量"];
    let switchTypeNameText = ["是否需要蒸汽地毯服务"];
    


    @IBOutlet weak var cleaningStyleSegement: UISegmentedControl!
    @IBOutlet weak var typeOfCleaningUITableView: UITableView!
    @IBAction func changeTypeOfCleaning(sender: UISegmentedControl) {
        //define a switch statement on switching the segment and table view.

    }


    
    //    let houseTypeDataSource = HouseTypeDataSource();
    override func viewDidLoad() {
        super.viewDidLoad();

    }

    // connecting the custom reusable cell to the table view.
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if (indexPath.row < tableViewDataSource.count) {
            let nameTextAndPickerViewStyleCell = tableView.dequeueReusableCellWithIdentifier("nameTextAndPickerViewStyleTableCell", forIndexPath: indexPath) as! nameTextAndPickerViewStyleTableCell;
            //setting value for the cell, be aware the cell contains pickerview, and pickerview set data in the cell class.
            nameTextAndPickerViewStyleCell.nameTextOfUITableCell.text = tableViewDataSource[indexPath.row];

            return nameTextAndPickerViewStyleCell;
        } else {
            let nameTextAndSwitchStyleCell = tableView.dequeueReusableCellWithIdentifier("nameTextAndSwichStyleTableCell") as! nameTextAndSwitchStyleTableViewCell;
            nameTextAndSwitchStyleCell.nameTextOfUITableCell.text = switchTypeNameText.first;
            return nameTextAndSwitchStyleCell;
        }
    }

    // MARK - tableView Setting
     func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableViewDataSource.count + switchTypeNameText.count;
    }
































}