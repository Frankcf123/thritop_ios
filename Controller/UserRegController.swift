//
//  UserRegController.swift
//  thriptop-ios
//
//  Created by Chen Fang on 27/11/2015.
//  Copyright © 2015 RMIT. All rights reserved.
//

import Foundation
import UIKit
import Alamofire

class UserRegController :UIViewController {
    
    let base_url="http://www.thritop.com.au/MasterAdmin/index.php/restful";
    let login_api="/loginapi"
    let register_api="/registerapi"
    let utility:Utility = Utility()

    @IBOutlet weak var fullName: UITextField!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var verCode: NSLayoutConstraint!
    
    @IBOutlet weak var phoneNum: UITextField!
    override func viewDidLoad() {
        //        callApi(register_api)
        print("thFDFissss is a test ")
        
    }
    
    func callApi(api_option:String,paramValue:[String]){
        
        Alamofire.request(.POST, base_url+api_option, parameters: [
            "c_name":paramValue[0],
            "c_phone":paramValue[1],
            "c_pass": paramValue[2]
            ])
            .responseJSON { response in
//                print(response.request)  // original URL request
//                print(response.response) // URL response
//                print(response.data)     // server data
//                print(response.result)   // result of response serialization
//                print("comEWREWes inside")
                if let JSON = response.result.value {
                    print("JSON: \(JSON)")
                    let result:String = JSON.objectForKey("result")! as! String
                    
                    print("result is \(result) fd")
                    
                    if(result.containsString("true")){
                        let username:String = JSON.objectForKey("c_name")! as! String
                        let id:String = JSON.objectForKey("c_id")! as! String

                        self.utility.showAlertBox(username)
                        self.utility.recordDefaultValue("c_name",value: username)
                        self.utility.recordDefaultValue("c_id",value: id)

//                        NSUserDefaults.standardUserDefaults().setObject(username, forKey: "username")
                    }else{
                        self.utility.showAlertBox("fail")
                    }

                }
        }
    }
    
   

    @IBAction func Register(sender: AnyObject) {
        registerUser()
    }
  
    func registerUser(){
        let name:String = self.fullName.text!
        let pwd:String = self.password.text!
        let phone_num:String = self.phoneNum.text!
        if(name.isEmpty || pwd.isEmpty || phone_num.isEmpty){
            print("Please complete all the field")
        }else{
            let params:[String] = [name,phone_num,pwd]
            let username = self.utility.getDefaultValue("c_id")
            if( username != "" ){
                self.utility.showAlertBox("user exists"+username)
            }else{
                callApi(register_api,paramValue: params)
            }
        }
//        return "fd"+name+pwd+ph
    }

}
