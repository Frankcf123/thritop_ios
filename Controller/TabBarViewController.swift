//
//  TabBarViewController.swift
//  thriptop-ios
//
//  Created by WEIQIANG LIANG on 28/11/2015.
//  Copyright © 2015 RMIT. All rights reserved.
//

import UIKit

class TabBarViewController : UITabBarController {
    let phoneNumber:String = "0416813850";
    let calltitle:String = "Book Now";
    let callAltertCallAction:String = "Call";
    let callAltertCancelAction:String = "Cancel";

    override func viewDidLoad() {
        super.viewDidLoad();
        //Due to we need to override the NavigationBar item, we need to make the title programmatically.
        navigationItem.rightBarButtonItem?.title = "Booking";

    }

    @IBAction func actionOnBooking(sender: UIBarButtonItem) {
        let callActionTitile:String = "Call: \(phoneNumber)";

        let alert=UIAlertController(title: calltitle, message: callActionTitile, preferredStyle: .Alert)

        //Call action
        let callAction = UIAlertAction(title: callAltertCallAction, style: .Default) {
            (action) in self.callPhoneNumber();
        }
        //Cancel action
        let cancelAction = UIAlertAction(title: callAltertCancelAction, style: .Default){
             (action) in print(action)
        };

        alert.addAction(callAction);
        alert.addAction(cancelAction);

        self.presentViewController(alert, animated: true){

        }

    }

    private func callPhoneNumber() {
        if (!self.phoneNumber.isEmpty) {
            let phoneCallURL:NSURL = NSURL(string: "tel://\(phoneNumber)")!;
            let application:UIApplication = UIApplication.sharedApplication();
            if(application.canOpenURL(phoneCallURL)) {
                application.openURL(phoneCallURL);
            }
        }

    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
//        if segue.identifier == "showDetail"  {
//            let destinationController = segue.destinationViewController as ShowTableViewController
//            destinationController.navigationItem.title = "摇到的菜"
//            destinationController.tempCaipin = self.showArray //传值
//        }
        
    }
}
