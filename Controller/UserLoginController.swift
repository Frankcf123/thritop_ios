//
//  UserLoginController.swift
//  thriptop-ios
//
//  Created by Chen Fang on 27/11/2015.
//  Copyright © 2015 RMIT. All rights reserved.
//

import Foundation
import UIKit
import Alamofire

class UserLoginController :UIViewController {
    
    let base_url="http://www.thritop.com.au/MasterAdmin/index.php/restful";
    let login_api="/loginapi"
    let register_api="/registerapi"
    let utility:Utility = Utility()
    @IBOutlet weak var phoneNoField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        //        callApi(register_api)
        
    }
    @IBAction func loginAction(sender: AnyObject) {
        self.loginUser()
    }
    
    func callApi(api_option:String,paramValue:[String]){
        
        Alamofire.request(.POST, base_url+api_option, parameters: [
            "c_phone":paramValue[0],
            "c_pass": paramValue[1]
            ])
            .responseJSON { response in
                //                print(response.request)  // original URL request
                //                print(response.response) // URL response
                //                print(response.data)     // server data
                //                print(response.result)   // result of response serialization
                //                print("comEWREWes inside")
                if let JSON = response.result.value {
                    print("JSON: \(JSON)")
//                    let test = JSON["c_name"]
                    let result:String = JSON.objectForKey("result")! as! String

                    print("result is \(result) fd")
                
                    if(result.containsString("true")){
                        let username:String = JSON.objectForKey("c_name")! as! String
                     
                        self.utility.showAlertBox(username)
//                        self.showAlertBox(username)
                    }else{
                        self.utility.showAlertBox("failure")
                    }
                }
        }
    }
   
  
    func loginUser(){
//        let name:String = self.fullName.text!
        let pwd:String = self.passwordField.text!
        let phone_num:String = self.phoneNoField.text!
        if( pwd.isEmpty || phone_num.isEmpty){
            print("Please complete all the field")
        }else{
            let params:[String] = [phone_num,pwd]
            callApi(login_api,paramValue: params)
        }
    }
}
