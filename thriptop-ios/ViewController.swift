//
//  ViewController.swift
//  thriptop-ios
//
//  Created by WEIQIANG LIANG on 14/11/2015.
//  Copyright © 2015 RMIT. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UIPageViewControllerDataSource{

    var pageViewController : UIPageViewController!
    var pageImages: NSArray!
    var pageTitles: NSArray!
    var timerIndex:Int = 0;
    var timerInitalIndexIndicator:Bool = true;

    let pageControl = UIPageControl.appearance();



    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.pageTitles = NSArray(objects: "Tower","WheelOfFortune","World");
        self.pageImages = NSArray(objects: "Tower.jpg","WheelOfFortune.jpg","World.jpg");

        self.pageViewController = self.storyboard!.instantiateViewControllerWithIdentifier("PageController") as! UIPageViewController;
        self.pageViewController.dataSource = self;

        setupContentViewSilder(0);
//        let timer = NSTimer.scheduledTimerWithTimeInterval(2, target: self, selector: "updateTimerEvent", userInfo: nil, repeats: true);

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    private func viewcontrollerAtIndex (index:Int) -> ContentViewController {

        if ((self.pageTitles.count == 0) || (index >= self.pageTitles.count)) {
            return ContentViewController();
        }

        let contentViewController: ContentViewController = self.storyboard!.instantiateViewControllerWithIdentifier("ContentViewController") as! ContentViewController;

        contentViewController.imageFile = pageImages[index] as! String;
        contentViewController.pageIndex = index;

        return contentViewController;
    }

    // MARK: page view controller data source

    func pageViewController(pageViewController: UIPageViewController, viewControllerAfterViewController viewController: UIViewController) -> UIViewController? {
        let pageViewController = viewController as! ContentViewController;
        var index = pageViewController.pageIndex as Int;

        if (index == NSNotFound) {
            return nil;
        }

        index++;

        if (index == self.pageTitles.count) {
            return nil
        }
        return self.viewcontrollerAtIndex(index);

    }

    func pageViewController(pageViewController: UIPageViewController, viewControllerBeforeViewController viewController: UIViewController) -> UIViewController? {

        let pageViewController = viewController as! ContentViewController;
        var index = pageViewController.pageIndex as Int;

        if (index == 0 || index == NSNotFound) {
            return nil
        }

        index--;
        return self.viewcontrollerAtIndex(index);
    }

    func presentationCountForPageViewController(pageViewController: UIPageViewController) -> Int {
        return self.pageTitles.count;
    }

    func presentationIndexForPageViewController(pageViewController: UIPageViewController) -> Int {
        return 0;
    }

    func updateTimerEvent() {
        if self.timerInitalIndexIndicator {
            timerIndex++;
            timerInitalIndexIndicator = false;

        } else {
            if timerIndex != 0 && timerIndex <= self.pageImages.count-1 {
                setupContentViewSilder(timerIndex);
                timerIndex++;
                timerInitalIndexIndicator = false;
            } else {
                timerIndex = 0;
                timerInitalIndexIndicator = true;
            }
        }


    }

    private func setupContentViewSilder(index:Int){
        if (index > self.pageImages.count-1){
            return;
        }
        let startVC = self.viewcontrollerAtIndex(index) as ContentViewController;
        let viewControllers = NSArray(object: startVC);

        self.pageViewController.setViewControllers(viewControllers as? [UIViewController], direction: .Forward, animated: true, completion: nil);

        self.addChildViewController(self.pageViewController);
        self.view.addSubview(self.pageViewController.view);
        self.pageViewController.didMoveToParentViewController(self);
    }

    
    
}

