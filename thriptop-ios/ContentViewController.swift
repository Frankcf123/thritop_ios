//
//  ContentViewController.swift
//  thriptop-ios
//
//  Created by WEIQIANG LIANG on 22/11/2015.
//  Copyright © 2015 RMIT. All rights reserved.
//

import UIKit
class ContentViewController:UIViewController {
    
    @IBOutlet weak var imageView: UIImageView!

    var pageIndex : Int!;
    var imageFile : String!;

    override func viewDidLoad() {
        super.viewDidLoad();
        self.imageView.image = UIImage(named: imageFile);

    }
}