//
//  HomeViewController.swift
//  thriptop-ios
//
//  Created by WEIQIANG LIANG on 21/11/2015.
//  Copyright © 2015 RMIT. All rights reserved.
//

import Foundation
import UIKit

class HomeViewController : ViewController {

    @IBOutlet weak var contentImageView: UIImageView!



    //variable
    var imageItemIndex:Int = 0;
    var imageItemName:String = "";

    override func viewDidLoad() {
        super.viewDidLoad();
        // control when the view is loaded.
        contentImageView.image = UIImage(named: imageItemName);

    }

    private func createPageViewController(){
        self.storyboard!.instantiateViewControllerWithIdentifier("PageController") as UIPageViewController
    }

    

}